# souvenir_shop
- In this project, I use MySQL database, so for running it you need to have installed MySQL Server on your PC.
- Create database by running script from src/main/resources/schema.sql
- Write your username and password in hibernate.cfg.xml from src/main/resources
- Run main() in class Main for work with souvenir shop.
- For stopping program write 'q' symbol in console.
- Write all requests like in a requirements.
