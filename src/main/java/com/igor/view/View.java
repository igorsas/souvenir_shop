package com.igor.view;

import com.igor.util.Parser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class View {
    private static final Logger LOG = LogManager.getLogger(View.class);
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void start() {
        String inputData = "";
        do {
            System.out.println("Please, enter your operation:");
            try {
                inputData = reader.readLine();
            } catch (IOException e) {
                LOG.error(e.getMessage());
            }
            System.out.println(Parser.parse(inputData));
        } while (!inputData.equalsIgnoreCase("q"));
    }
}
