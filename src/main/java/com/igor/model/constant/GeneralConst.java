package com.igor.model.constant;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

public class GeneralConst {
    private static final String PATTERN = "yyyy-MM-dd";
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(PATTERN);

    public static final int STARTED_DATE_INDEX_IN_PURCHASE_REQUEST = 9;
    public static final int STARTED_DATE_INDEX_IN_REPORT_REQUEST = 7;

    public static final List<String> AVAILABLE_CURRENCY = Arrays.asList(
            "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD",
            "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BHD", "BIF",
            "BMD", "BND", "BOB", "BRL", "BSD", "BTC", "BTN", "BWP",
            "BYR", "BYN", "BZD", "CAD", "CDF", "CHF", "CLF", "CLP",
            "CNY", "COP", "CRC", "CUC", "CUP", "CVE", "CZK", "DJF",
            "DKK", "DOP", "DZD", "EGP", "ERN", "ETB", "EUR", "FJD",
            "FKP", "GBP", "GEL", "GGP", "GHS", "GIP", "GMD", "GNF",
            "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR",
            "ILS", "IMP", "INR", "IQD", "IRR", "ISK", "JEP", "JMD",
            "JOD", "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KRW",
            "KWD", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL",
            "LTL", "LVL", "LYD", "MAD", "MDL", "MGA", "MKD", "MMK",
            "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MYR",
            "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "OMR",
            "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR",
            "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SDG",
            "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "STD", "SVC",
            "SYP", "SZL", "THB", "TJS", "TMT", "TND", "TOP", "TRY",
            "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "UYU", "UZS",
            "VEF", "VND", "VUV", "WST", "XAF", "XAG", "XAU", "XCD",
            "XDR", "XOF", "XPF", "YER", "ZAR", "ZMK", "ZMW", "ZWL");

    public static final String FIXER_API_ACCESS_KEY = "de0ef91b8aa3432e3768698a983cf956";
    public static final String BASE_URL = "http://data.fixer.io/api/";
    public static final String ACCESS_KEY_IN_URL = "?access_key=";
    public static final String CURRENCY_SYMBOL_IN_URL = "&symbols=";


    private GeneralConst() {

    }
}
