package com.igor.util;

import com.igor.dao.PurchaseDao;
import com.igor.model.entity.Purchase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.igor.model.constant.GeneralConst.*;

public class Parser {
    private static final Logger LOG = LogManager.getLogger(Parser.class);

    public static String parse(String request) {
        String message = "Incorrect Data";
        if (request.startsWith("purchase")) {
            try {
                return writeToDB(request);
            } catch (ParseException e) {
                LOG.error(e.getMessage());
                message = "Incorrect Date format";
            } catch (NumberFormatException e) {
                LOG.error(e.getMessage());
                message = "Incorrect Sum";
            } catch (NoSuchFieldException e) {
                message = "Your currency is unavailable";
                LOG.error(message);
            }
        } else if (request.equals("all")) {
            return getAllPurchases();
        } else if (request.startsWith("clear")) {
            try {
                return clearByDate(request);
            } catch (ParseException e) {
                LOG.error(e.getMessage());
                message = "Incorrect Date format";
            }
        } else if (request.startsWith("report")) {
            try {
                return writeReportForYear(request);
            } catch (NumberFormatException e) {
                LOG.error(e.getMessage());
                message = "Incorrect year";
            } catch (NoSuchFieldException e) {
                message = "Your currency is unavailable";
                LOG.error(message);
            } catch (IOException e) {
                LOG.error(e.getMessage());
            }
        }
        return message;
    }

    private static String getAllPurchases() {
        List<Purchase> purchases = new PurchaseDao().getAll();
        if (purchases.isEmpty()) {
            return "There's no any purchases\n";
        }
        purchases.sort(compareByDate);
        Date date = purchases.get(0).getDate();
        StringBuilder result = new StringBuilder(SIMPLE_DATE_FORMAT.format(date)).append("\n");
        for (Purchase currentPurchase : purchases) {
            if (!currentPurchase.getDate().equals(date)) {
                date = currentPurchase.getDate();
                result.append(SIMPLE_DATE_FORMAT.format(date)).append("\n");
            }
            result.append(currentPurchase.getName()).append(" ").
                    append(Math.round(currentPurchase.getSum() * 100.0) / 100.0).append(" ").
                    append(currentPurchase.getCurrency()).append("\n");

        }
        return String.valueOf(result);
    }

    private static String writeReportForYear(String request) throws NumberFormatException, NoSuchFieldException, IOException {
        request = request.substring(STARTED_DATE_INDEX_IN_REPORT_REQUEST);
        String[] resultItem = getItem(request);
        int year = Integer.parseInt(resultItem[0]);
        if (year < 0) {
            throw new NumberFormatException();
        }
        request = resultItem[1];
        resultItem = getItem(request);
        String currency = resultItem[0];
        if (!Currency.currencyIsAvailable(currency)) {
            throw new NoSuchFieldException();
        }
        List<Purchase> purchases = new PurchaseDao().getByYear(year);
        if (purchases.isEmpty()) {
            return "There's no any purchases\n";
        }
        double report = 0;
        for (Purchase purchase : purchases) {
            report += Currency.exchange(purchase.getSum(), purchase.getCurrency(), currency, SIMPLE_DATE_FORMAT.format(purchase.getDate()));
        }
        return Math.round(report * 100.0) / 100.0 + " " + currency;
    }

    private static String clearByDate(String request) throws ParseException {
        request = getItem(request)[1];
        Date date = SIMPLE_DATE_FORMAT.parse(getItem(request)[0]);
        new PurchaseDao().deleteByDate(date);
        return getAllPurchases();
    }

    private static String writeToDB(String request) throws ParseException, NumberFormatException, NoSuchFieldException {
        request = request.substring(STARTED_DATE_INDEX_IN_PURCHASE_REQUEST);
        String[] resultItem = getItem(request);
        Date date = SIMPLE_DATE_FORMAT.parse(resultItem[0]);
        request = resultItem[1];
        resultItem = getItem(request);
        double sum = Double.parseDouble(resultItem[0]);
        if (sum < 0) {
            throw new NumberFormatException();
        }
        request = resultItem[1];
        resultItem = getItem(request);
        String currency = resultItem[0];
        if (!Currency.currencyIsAvailable(currency)) {
            throw new NoSuchFieldException();
        }
        request = resultItem[1];
        String purchaseName = request.replace('"', Character.MIN_VALUE);
        Purchase purchase = new Purchase(purchaseName, sum, currency, date);
        new PurchaseDao().save(purchase);
        return getAllPurchases();
    }

    private static String[] getItem(String request) {
        for (int i = 0; i < request.length(); i++) {
            if (request.charAt(i) == ' ') {
                String item = request.substring(0, i);
                request = request.substring(i + 1);
                return new String[]{item, request};
            }
        }
        return new String[]{request, ""};
    }

    private static Comparator<Purchase> compareByDate = Comparator.comparing(Purchase::getDate);
}
