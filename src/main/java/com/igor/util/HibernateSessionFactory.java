package com.igor.util;

import com.igor.model.entity.Purchase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.Objects;

public class HibernateSessionFactory {
    private static SessionFactory sessionFactory;
    private static final Logger LOG = LogManager.getLogger(HibernateSessionFactory.class);

    private HibernateSessionFactory() {
    }

    public static SessionFactory getSessionFactory() {
        if (Objects.isNull(sessionFactory)) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(Purchase.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            } catch (Throwable e) {
                LOG.error(e.getMessage());
            }
        }
        return sessionFactory;
    }
}
