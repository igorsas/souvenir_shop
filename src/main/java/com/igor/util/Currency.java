package com.igor.util;

import org.json.JSONObject;

import java.io.IOException;

import static com.igor.model.constant.GeneralConst.*;

public class Currency {
    public static boolean currencyIsAvailable(String currency) {
        return AVAILABLE_CURRENCY.contains(currency);
    }

    public static double exchange(double sum, String currency1, String currency2, String date) throws IOException {
        return (sum / getExchangeRateToEUR(currency1, date)) * getExchangeRateToEUR(currency2, date);
    }

    private static double getExchangeRateToEUR(String currency, String date) throws IOException {
        JSONObject json = JsonReader.readJsonFromUrl(BASE_URL + date + ACCESS_KEY_IN_URL + FIXER_API_ACCESS_KEY + CURRENCY_SYMBOL_IN_URL + currency);
        JSONObject ratesJson = json.getJSONObject("rates");
        return ratesJson.getDouble(currency);
    }
}
