package com.igor.dao;

import com.igor.model.entity.Purchase;
import com.igor.util.HibernateSessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Date;
import java.util.List;

import static com.igor.model.constant.GeneralConst.SIMPLE_DATE_FORMAT;

public class PurchaseDao {
    public Purchase getById(int id) {
        return HibernateSessionFactory.getSessionFactory().openSession().get(Purchase.class, id);
    }

    public void save(Purchase t) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(t);
        transaction.commit();
        session.close();
    }

    public void update(Purchase t) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(t);
        transaction.commit();
        session.close();
    }

    public void delete(Purchase t) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(t);
        transaction.commit();
        session.close();
    }

    public void deleteByDate(Date date) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.createQuery("delete Purchase where date = " +
                "" + "'" + SIMPLE_DATE_FORMAT.format(date) + "'").executeUpdate();
        transaction.commit();
        session.close();
    }

    public List<Purchase> getAll() {
        return (List<Purchase>) HibernateSessionFactory.getSessionFactory().openSession().
                createQuery("FROM Purchase").list();
    }

    public List<Purchase> getByYear(int year) {
        return (List<Purchase>) HibernateSessionFactory.getSessionFactory().openSession().
                createQuery("FROM Purchase WHERE year(date)=" + year).list();
    }

}
