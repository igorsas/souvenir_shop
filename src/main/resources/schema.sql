-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema souvenir_shop
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `souvenir_shop`;

-- -----------------------------------------------------
-- Schema souvenir_shop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `souvenir_shop` DEFAULT CHARACTER SET utf8;
USE `souvenir_shop`;

-- -----------------------------------------------------
-- Table `souvenir_shop`.`purchase`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `souvenir_shop`.`purchase`;

CREATE TABLE IF NOT EXISTS `souvenir_shop`.`purchase`
(
    `id`       INT AUTO_INCREMENT,
    `name`     VARCHAR(45)    NOT NULL,
    `sum`      DECIMAL(10, 2) NOT NULL,
    `currency` VARCHAR(3)     NOT NULL,
    `date`     DATE           NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC)
)
    ENGINE = InnoDB;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
