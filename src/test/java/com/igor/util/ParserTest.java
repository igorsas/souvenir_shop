package com.igor.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParserTest {
    private final String INCORRECT_DATA = "Incorrect Data";
    private final String INCORRECT_DATE = "Incorrect Date format";
    private final String INCORRECT_YEAR = "Incorrect year";
    private final String INCORRECT_SUM = "Incorrect Sum";
    private final String UNAVAILABLE_CURRENCY = "Your currency is unavailable";

    @Test
    void parseEmptyRequest() {
        assertEquals(Parser.parse(""), INCORRECT_DATA);
    }

    @Test
    void parseIncorrectDateFormat() {
        assertEquals(Parser.parse("purchase 201.11.11 20 UAH some"), INCORRECT_DATE);
        assertEquals(Parser.parse("clear 2019.04.25"), INCORRECT_DATE);
        assertEquals(Parser.parse("report -1000 UAH"), INCORRECT_YEAR);
    }

    @Test
    void parseIncorrectSum() {
        assertEquals(Parser.parse("purchase 2011-11-11 aaa UAH some"), INCORRECT_SUM);
        assertEquals(Parser.parse("purchase 2011-11-11 -100 UAH some"), INCORRECT_SUM);
    }

    @Test
    void parseUnavailableCurrency() {
        assertEquals(Parser.parse("purchase 2011-11-11 100 GRYVNIA some"), UNAVAILABLE_CURRENCY);
        assertEquals(Parser.parse("report 2019 GRYVNIA"), UNAVAILABLE_CURRENCY);
    }
}